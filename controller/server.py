from flask import Flask, request
import json
import tensorflow as tf
from keras.models import load_model
import numpy as np
from PIL import Image
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

model = load_model("../modal/model.h5")

def predict_image(input_image):
    img = tf.keras.preprocessing.image.load_img(input_image, grayscale=True, target_size=(48, 48))
    x = tf.keras.preprocessing.image.img_to_array(img)
    x = np.expand_dims(x, axis = 0)
    x/=255
    emotion_objects = ['angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral']
    custom = model.predict(x)

    float_values = custom.round(decimals=3)
    float_values = float_values.tolist()
    for x in float_values:
        float_values = x

    return emotion_objects, float_values

@app.route('/predict', methods=['GET','POST'])
def return_prediction():
    input_image = request.files['image']
    emotion_objects, values = predict_image(input_image)
    data = {}
    
    for x, y in zip(emotion_objects, values):
        data[x] = y

    return json.dumps(data)


if(__name__ == "__main__"):
    app.run(threaded=False)