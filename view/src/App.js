import React, { Component } from "react";
import { Button } from "@material-ui/core";
import styles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import TabComponent from "./Components/Tab/TabComponent";
import ImageCardComponent from "./Components/ImageCard/ImageCardComponent";
import TextCardComponent from "./Components/TextCard/TextCardComponent";
import axios from "axios";

class App extends Component {
  constructor() {
    super();
    this.state = {
      emotions: [],
      value: 0,
      image: false,
      selectedImage: null,
      ImageUrl: null,
    };
  }

  render() {
    const { classes } = this.props;

    return (
      <div align="center">
        <TabComponent />

        {this.state.image === true ? (
          <ImageCardComponent
            picture={this.state.ImageUrl}
            emotions={this.state.emotions}
          />
        ) : (
          <TextCardComponent />
        )}
        <input
          style={{ display: "none" }}
          type="file"
          accept="image/png, image/jpeg"
          onChange={this.fileChangedHandler}
          ref={(fileInput) => (this.fileInput = fileInput)}
        />
        <br />
        <Button className={classes.btn} onClick={() => this.fileInput.click()}>
          Select Photo
        </Button>
      </div>
    );
  }

  fileChangedHandler = (event) => {
    if (event.target.files[0] !== undefined) {
      this.setState(
        {
          selectedImage: event.target.files[0],
          ImageUrl: URL.createObjectURL(event.target.files[0]),
          image: true,
        },
        () => {
          const fd = new FormData();
          fd.append(
            "image",
            this.state.selectedImage,
            this.state.selectedImage.name
          );
          axios
            .post("http://127.0.0.1:5000/predict", fd, {
              headers: {
                "content-type": "multipart/form-data",
              },
            })
            .then((res) => {
              this.setState({ emotions: res.data });
            })
            .catch((err) => console.log(err));
        }
      );
    }
  };
}

export default withStyles(styles)(App);
