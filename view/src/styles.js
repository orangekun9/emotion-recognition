const styles = (theme) => ({
  btn: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    border: 0,
    borderRadius: 3,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "white",
    height: 48,
    padding: "0 30px",
    marginTop: 50,
    '&:hover': {
      boxShadow: "0 3px 30px 8px rgba(255, 105, 135, .3)",
      background: "linear-gradient(45deg, #fe809b 30%, #ff9c66 90%)",
    },
    '&:active': {
      boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)"
    }
  },
});

export default styles;
