import React from 'react';
import Card from "@material-ui/core/Card";
import styles from "./styles";
import { withStyles } from "@material-ui/core/styles";
// import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from '@material-ui/core/Typography';
import 'typeface-roboto';
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import grey from "@material-ui/core/colors/grey";

class TextCardComponent extends React.Component {
    // eslint-disable-next-line
    constructor() {
        super();
    }

    render() {
        const { classes } = this.props;

        const theme = createMuiTheme({
            palette: {
                primary: {
                    main: grey[300]
                },
                secondary: {
                    main: grey[400]
                }
            }
        });

        return (
            <div align="center">
                <Card className={classes.root}>
                    <CardContent>
                        <MuiThemeProvider theme={theme}>
                            <Typography variant="h4" component="h2" color="primary">
                                No Image Found!
                            </Typography>
                            <br />
                            <br />
                            <br />
                            <Typography variant="body1" component="p" color="secondary">
                                Please select an image from your system to get detect its emotions, make sure that the image contains one person otherwise you won't get accurate results.
                            </Typography>
                            <br />
                            <Typography variant="body1" component="p" color="secondary">
                                Please click the button below and select you image.
                            </Typography>
                        </MuiThemeProvider>
                    </CardContent>
                </Card>
            </div>
        )
    }
}
export default withStyles(styles)(TextCardComponent);
