const styles = (theme) => ({
    navBar: {
        flexGrow: 1,
        maxWidth: 600,
        background: "#333333",
        boxShadow: "0 3px 5px 2px",
    },
});
export default styles;