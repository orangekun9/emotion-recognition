import React from 'react';
import styles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import red from "@material-ui/core/colors/red";

class TabComponent extends React.Component {
    constructor() {
        super();
        this.state = {
            value: 0
        }
    }

    render() {
        const { classes } = this.props;

        const theme = createMuiTheme({
            palette: {
                secondary: {
                    main: red[300],
                },
            },
        });

        return (
            <MuiThemeProvider theme={theme}>
                <Paper square className={classes.navBar}>
                    <Tabs
                        value={this.state.value}
                        onChange={(e, val) => this.setValue(val)}
                        variant="fullWidth"
                        TabIndicatorProps={{
                            style: {
                                background:
                                    "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
                            },
                        }}
                        textColor="secondary"
                        aria-label="Customized Tab-bar"
                    >
                        <Tab icon={<InsertEmoticonIcon />} label="Emotion Recognition" />
                    </Tabs>
                </Paper>
            </MuiThemeProvider>
        );
    }
    setValue = (val) => this.setState({ value: val });
}

export default withStyles(styles)(TabComponent);