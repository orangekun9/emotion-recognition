const styles = (theme) => ({
    card_1: {
        width: 500,
        height: 600,
        marginTop: 50,
        background: "#333333"
    },

    media: {
        height: 0,
        paddingTop: '56.25%',
    },
})

export default styles;