import React from 'react';
import Card from "@material-ui/core/Card";
import styles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import AngryProgressBarComponent from "../ProgressBar/Angry"
import DisgustProgressBarComponent from "../ProgressBar/Disgust"
import FearProgressBarComponent from "../ProgressBar/Fear"
import HappyProgressBarComponent from "../ProgressBar/Happy"
import NeutralProgressBarComponent from "../ProgressBar/Neutral"
import SadProgressBarComponent from "../ProgressBar/Sad"
import SurpriseProgressBarComponent from "../ProgressBar/Surprise"
import Grid from '@material-ui/core/Grid';
import grey from "@material-ui/core/colors/grey";

class ImageCardComponent extends React.Component {
    // eslint-disable-next-line
    constructor() {
        super();
    }

    render() {

        const { classes, picture, emotions } = this.props;

        var results = []
        for (var key in emotions) {
            results.push(emotions[key]);
        }

        const theme = createMuiTheme({
            palette: {
                primary: {
                    main: grey[300]
                },
                secondary: {
                    main: grey[400]
                }
            }
        });

        return (
            <div align="center">
                <Card className={classes.card_1}>
                    <CardMedia
                        className={classes.media}
                        image={picture}
                        title="User Image"
                    />
                    <CardContent>
                        <MuiThemeProvider theme={theme}>
                            <Grid container spacing={2}>
                                <Grid item xs={3}>
                                    <Typography variant="body1" component="p" color="secondary">
                                        Angry
                                    </Typography>

                                </Grid>
                                <Grid item xs={9}>
                                    <AngryProgressBarComponent result={results[0]} />
                                </Grid>

                                <Grid item xs={3}>
                                    <Typography variant="body1" component="p" color="secondary">
                                        Disgust
                                    </Typography>
                                </Grid>
                                <Grid item xs={9}>
                                    <DisgustProgressBarComponent result={results[1]} />
                                </Grid>

                                <Grid item xs={3}>
                                    <Typography variant="body1" component="p" color="secondary">
                                        Fear
                                    </Typography>
                                </Grid>
                                <Grid item xs={9}>
                                    <FearProgressBarComponent result={results[2]} />
                                </Grid>

                                <Grid item xs={3}>
                                    <Typography variant="body1" component="p" color="secondary">
                                        Happy
                                    </Typography>
                                </Grid>
                                <Grid item xs={9}>
                                    <HappyProgressBarComponent result={results[3]} />
                                </Grid>

                                <Grid item xs={3}>
                                    <Typography variant="body1" component="p" color="secondary">
                                        Sad
                                    </Typography>
                                </Grid>
                                <Grid item xs={9}>
                                    <SadProgressBarComponent result={results[4]} />
                                </Grid>


                                <Grid item xs={3}>
                                    <Typography variant="body1" component="p" color="secondary">
                                        Surprise
                                    </Typography>
                                </Grid>
                                <Grid item xs={9}>
                                    <SurpriseProgressBarComponent result={results[5]} />
                                </Grid>

                                <Grid item xs={3}>
                                    <Typography variant="body1" component="p" color="secondary">
                                        Neutral
                                    </Typography>
                                </Grid>
                                <Grid item xs={9}>
                                    <NeutralProgressBarComponent result={results[6]} />
                                </Grid>
                            </Grid>
                        </MuiThemeProvider>
                    </CardContent>
                </Card>
            </div>
        )
    }
}
export default withStyles(styles)(ImageCardComponent);