import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import grey from "@material-ui/core/colors/grey";

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "10px",
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        }
    },
    bar1Determinate: {
        backgroundColor: grey[700],
    },
    determinate: {
        backgroundColor: "#333333",
    }
}));

export default function FearProgressBarComponent(props) {
    const classes = useStyles();
    const [completed, setCompleted] = React.useState(0);
    const [buffer, setBuffer] = React.useState(10);
    const limit = props.result * 100;

    const progress = React.useRef(() => { });
    React.useEffect(() => {
        progress.current = () => {
            if (completed <= limit) {
                const diff1 = Math.random() * 10;
                setCompleted(completed + 1);
                setBuffer(completed + diff1);
            }
            else if (limit + 1 < completed) {
                setCompleted(0);
            }
            else {
                setBuffer(0);
            }
        };
    });

    React.useEffect(() => {
        function tick() {
            progress.current();
        }
        const timer = setInterval(tick, 100);

        return () => {
            clearInterval(timer);
        };
    }, []);

    return (
        <div className={classes.root}>
            <LinearProgress classes={{ bar1Determinate: classes.bar1Determinate, determinate: classes.determinate }} variant="determinate" value={completed} valueBuffer={buffer} />
        </div>
    );
}